package com.example.test.app;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import com.example.test.app.database.DatabaseHelper;
import com.example.test.app.database.OrmLiteBaseActivity;
import com.example.test.app.fragments.CustomSearchFragment;
import com.example.test.app.fragments.FavoritesFragment;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.util.List;


public class MainActivity extends OrmLiteBaseActivity<DatabaseHelper> {

    private ViewPager mPager;
    private ScreenSlidePagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initImageLoader();

        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        setTitle(getString(R.string.search));
                        break;
                    case 1:
                        setTitle(getString(R.string.favorites));
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setTitle(getString(R.string.search));
    }

    private void initImageLoader() {
        DisplayImageOptions.Builder displayImageOptionsBuilder = new DisplayImageOptions.Builder().bitmapConfig(Bitmap.Config.ARGB_8888)
                .cacheOnDisk(true).cacheInMemory(true)
                .decodingOptions(new BitmapFactory.Options()).imageScaleType(ImageScaleType.NONE);

//        ImageLoader.getInstance().init(new ImageLoaderConfiguration.Builder(getApplicationContext()).imageDownloader(
//                new AuthImageLoader(getApplicationContext())).build());
        ImageLoader.getInstance().init(new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(displayImageOptionsBuilder.build()).build());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
       List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragmentList) {
            if (((mPager.getCurrentItem() == 0 && fragment.getClass() == CustomSearchFragment.class)
                    || (mPager.getCurrentItem() == 1 && fragment.getClass() == FavoritesFragment.class))
                    && fragment.getChildFragmentManager().getBackStackEntryCount() > 0) {
                fragment.getChildFragmentManager().popBackStack();
                return;
            }
        }
        super.onBackPressed();
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private static final int NUM_PAGES = 2;

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new CustomSearchFragment();
                case 1:
                    return new FavoritesFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}
