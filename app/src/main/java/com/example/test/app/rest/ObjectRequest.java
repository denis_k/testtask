package com.example.test.app.rest;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.example.test.app.L;
import com.example.test.app.parsers.Parser;

public class ObjectRequest<T> extends BaseRequest<T> {
    private static final String TAG = ObjectRequest.class.getName();
    protected Parser<T> parser;

    public ObjectRequest(int method, String url, Parser<T> parser, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        this.parser = parser;
        L.log("url", url);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        if (response.data != null && response.data.length > 0) {
            //L.logLong("response", new String(response.data));
            if (parser == null) {
                return Response.success(null, enforceClientCaching(HttpHeaderParser.parseCacheHeaders(response), response));
            } else {
                return Response.success(parser.parse(response.data),
                        enforceClientCaching(HttpHeaderParser.parseCacheHeaders(response), response));
            }
        } else {
            return Response.error(new VolleyError("empty response!"));
        }
    }
}