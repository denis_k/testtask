package com.example.test.app.parsers;

import com.example.test.app.models.Result;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by korotenko on 09.12.14.
 */
public class ListParser extends Parser<List<Result>> {

    public static final String ITEMS = "items";
    public static final String TITLE = "title";
    public static final String SRC = "src";
    public static final String PAGEMAP = "pagemap";
    public static final String CSE_THUMBNAIL = "cse_thumbnail";
    public static final String CSE_IMAGE = "cse_image";
    public static final String CACHE_ID = "cacheId";

    @Override
    protected List<Result> parse(JsonNode node) {
        List<Result> list = new ArrayList<Result>();

        Iterator<JsonNode> iterator = node.path(ITEMS).iterator();
        while (iterator.hasNext()) {
            JsonNode nodeItem = iterator.next();
            Result item = new Result();
            item.cacheId = nodeItem.path(CACHE_ID).asText();
            item.name = nodeItem.path(TITLE).asText();
            Iterator<JsonNode> images = nodeItem.path(PAGEMAP).path(CSE_IMAGE).iterator();
            if (images.hasNext()) {
                item.imageUrl = images.next().path(SRC).asText();
            }
            Iterator<JsonNode> imagesT = nodeItem.path(PAGEMAP).path(CSE_THUMBNAIL).iterator();
            if (imagesT.hasNext()) {
                item.thumbnailUrl = imagesT.next().path(SRC).asText();
            }
            list.add(item);
        }
        return list;
    }
}
