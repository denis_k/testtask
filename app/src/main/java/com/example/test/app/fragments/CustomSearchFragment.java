package com.example.test.app.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.test.app.BusHelper;
import com.example.test.app.MainActivity;
import com.example.test.app.models.Result;
import com.example.test.app.parsers.ListParser;
import com.example.test.app.rest.ApiHelper;
import com.example.test.app.rest.ObjectRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.nostra13.universalimageloader.utils.L;
import com.squareup.otto.Subscribe;

import java.util.List;

public class CustomSearchFragment extends ScreenSlidePageFragment implements Response.Listener<List<Result>>, Response.ErrorListener {
    static final String TAG = CustomSearchFragment.class.getSimpleName();

    static final String API_KEY = "AIzaSyAg2ScxSewfmyMTUIRubr2Vy814nOa9Fvc";
    static final String NUM = "10";
    static final String URL = "https://www.googleapis.com/customsearch/v1?key=" + API_KEY + "&cx=013036536707430787589:_pqjad5hr1a&alt=json&q=%1$s&start=%2$s&num=" + NUM;

    private ProgressDialog dialog;
    private boolean isLoading = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusHelper.INSTANCE.register(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ApiHelper.init(activity);
    }

    protected void onSearch(final String query) {
        if (dialog == null) {
            dialog = new ProgressDialog(getActivity());
        }
        dialog.show();
        ApiHelper.cancelAll();
        final ListParser listParser = new ListParser() {
            @Override
            protected List<Result> parse(JsonNode node) {
                List<Result> list = super.parse(node);
                RuntimeExceptionDao<Result, Integer> dao = ((MainActivity) getActivity()).getHelper().getResultDao();
                List<Result> favorites = dao.queryForAll();
                for (Result result : list) {
                    for (Result favorite : favorites) {
                        if (result.cacheId.equals(favorite.cacheId)) {
                            result.isFav = true;
                            break;
                        }
                    }
                }
                return list;
            }
        };
        ApiHelper.request(new ObjectRequest(Request.Method.GET, String.format(URL, query, 1), listParser, this, this));

        recyclerView.addOnScrollListener(new OnPageLoadListener() {
            @Override
            public void loadMore(int count) {
                dialog.show();
                if (!isLoading) {
                    isLoading = true;
                    ApiHelper.request(new ObjectRequest(Request.Method.GET, String.format(URL, query, count + 1), listParser, CustomSearchFragment.this, CustomSearchFragment.this));
                }
            }
        });
    }

    @Override
    public void onResponse(List<Result> response) {
        isLoading = false;
        addAll(response);
        dialog.hide();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        isLoading = false;
        if (error.networkResponse != null && error.networkResponse.data != null) {
            L.i(TAG, new String(error.networkResponse.data));
        }
        dialog.hide();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Subscribe
    public void onFavoriteChanged(BusHelper.OnFavoriteChanged event) {
        List<Result> list = adapter.getList();
        for (Result item : list) {
            if (item.cacheId.equals(event.getResult().cacheId)) {
                item.isFav = event.getResult().isFav;
                break;
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onRefresh() {

    }
}