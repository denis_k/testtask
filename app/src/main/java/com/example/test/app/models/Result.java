package com.example.test.app.models;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by denis on 8/24/15.
 */
public class Result {
    @DatabaseField(generatedId = true)
    int id;
    @DatabaseField
    public String cacheId = "";
    @DatabaseField
    public String imageUrl = "";
    @DatabaseField
    public String thumbnailUrl = "";
    @DatabaseField
    public String name = "";
    public boolean isFav;

}
