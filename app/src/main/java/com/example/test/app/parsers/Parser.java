package com.example.test.app.parsers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public abstract class Parser <T> {

    public T parse(byte[] data) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode jsonNode = mapper.readTree(data);
            T result = parse(jsonNode);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public T parse(String data) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode jsonNode = mapper.readTree(data);
            T result = parse(jsonNode);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected abstract T parse(JsonNode node);

    public Error errorParser(byte[] data){
        return null;
    }

    public class Error {
        public String message = "";
        public boolean isInternal = false;

        public Error() {
        }

        public Error(String message) {
            this.message = message;
        }
    }
}