package com.example.test.app.fragments;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.View;
import com.example.test.app.BusHelper;
import com.example.test.app.MainActivity;
import com.example.test.app.models.Result;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class FavoritesFragment extends ScreenSlidePageFragment implements LoaderManager.LoaderCallbacks<List<Result>> {
    static final int LOADER_TIME_ID = 1;
    private List<Result> list;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusHelper.INSTANCE.register(this);
        getLoaderManager().initLoader(LOADER_TIME_ID, null, this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getLoaderManager().getLoader(LOADER_TIME_ID).forceLoad();
        swipeRefreshLayout.setEnabled(true);
    }

    protected void onSearch(String query) {
        if (list == null) {
            return;
        }
        List<Result> data = new ArrayList<Result>();
        for (Result item : list) {
            if (item.name.toLowerCase().contains(query.toLowerCase())) {
                data.add(item);
            }
        }
        addAll(data);
    }

    @Override
    public Loader<List<Result>> onCreateLoader(int id, Bundle args) {
        Loader<List<Result>> loader = null;
        if (id == LOADER_TIME_ID) {
            loader = new AsyncTaskLoader<List<Result>>(getActivity()) {
                @Override
                public List<Result> loadInBackground() {
                    RuntimeExceptionDao<Result, Integer> dao = ((MainActivity) getActivity()).getHelper().getResultDao();
                    List<Result> list = dao.queryForAll();
                    for (Result result : list) {
                        result.isFav = true;
                    }
                    return list;
                }
            };
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<Result>> loader, List<Result> data) {
        swipeRefreshLayout.setRefreshing(false);
        list = data;
        clearList();
        addAll(data);
    }

    @Override
    public void onLoaderReset(Loader<List<Result>> loader) {
    }

    @Subscribe
    public void onFavoriteChanged(BusHelper.OnFavoriteChanged event) {
        if (list != null) {
            if (event.getResult().isFav) {
                list.add(event.getResult());
            } else {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).cacheId.equals(event.getResult().cacheId)) {
                        list.remove(i);
                        break;
                    }
                }
            }
            clearList();
            addAll(list);
        }
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        getLoaderManager().getLoader(LOADER_TIME_ID).forceLoad();
    }
}