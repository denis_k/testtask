package com.example.test.app.fragments;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.example.test.app.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

public class InfoFragment extends Fragment {

    public static final String IMAGE_URL = "image_url";
    public String imageUrl = "";

    public static InfoFragment newInstance(String imageUrl) {
        InfoFragment fragment = new InfoFragment();
        Bundle bundle = new Bundle();
        bundle.putString(IMAGE_URL, imageUrl);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            imageUrl = bundle.getString(IMAGE_URL);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ImageView imageView = (ImageView) inflater.inflate(
                R.layout.fragment_info, container, false);
        ImageLoader.getInstance().displayImage(imageUrl, imageView, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, final View view, Bitmap loadedImage) {
                if (loadedImage != null) {
                    Palette.generateAsync(loadedImage, new Palette.PaletteAsyncListener() {
                        @Override
                        public void onGenerated(Palette palette) {
                            int color = palette.getVibrantColor(Color.BLACK);
                            view.setBackgroundColor(color);
                            //setTextColor(palette.getLightMutedColor(R.color.white));
                        }
                    });
                }
            }
        });
        return imageView;
    }
}