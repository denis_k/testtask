package com.example.test.app.rest;

import android.content.Context;
import com.android.volley.*;
import com.android.volley.toolbox.Volley;

/**
 * Created by korotenko on 29.07.14.
 */
public class ApiHelper extends RequestQueue {
    private static final String TAG = ApiHelper.class.getName();

    public static final int MY_SOCKET_TIMEOUT_MS = 100000;
    private static final Object REQUEST_QUEUE_LOCK = new Object();

    private static Context mContext;
    private static volatile RequestQueue queue;

    public ApiHelper(Cache cache, Network network, int threadPoolSize, ResponseDelivery delivery) {
        super(cache, network, threadPoolSize, delivery);
    }

    public ApiHelper(Cache cache, Network network, int threadPoolSize) {
        super(cache, network, threadPoolSize);
    }

    public ApiHelper(Cache cache, Network network) {
        super(cache, network);
    }

    public static RequestQueue getRequestQueue() {
        synchronized (REQUEST_QUEUE_LOCK) {
            if (queue == null) {
                queue = Volley.newRequestQueue(mContext);
            }
            return queue;
        }
    }

    public static void init(Context context) {
        mContext = context.getApplicationContext() == null ? context : context.getApplicationContext();
    }

    public static void request(Request request) {
        getRequestQueue().add(request);
    }

    public static void cancelAll() {
        getRequestQueue().cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
    }

}
