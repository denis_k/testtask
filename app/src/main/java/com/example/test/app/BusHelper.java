package com.example.test.app;

import com.example.test.app.models.Result;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

public class BusHelper {
    public static final Bus INSTANCE = new Bus(ThreadEnforcer.ANY);

    public static class OnFavoriteChanged {
        private Result result;

        public Result getResult() {
            return result;
        }

        public OnFavoriteChanged(Result result) {
            this.result = result;
        }
    }
}