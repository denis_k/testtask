package com.example.test.app.rest;

import android.util.Log;
import com.android.volley.*;
import com.android.volley.toolbox.HttpHeaderParser;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseRequest<T> extends Request<T> {
    public static final int MY_SOCKET_TIMEOUT_MS = 100000;
    protected static final int defaultClientCacheExpiry = 1000 * 60 * 5; // milliseconds; = 5 min
    protected final Map<String, String> mHeaders;
    protected final Response.Listener<T> listener;

    //    public static BaseRequest newGetRequest(String url, Response.Listener<T> listener, Response.ErrorListener errorListener) {
//        return new BaseRequest(Method.GET, url, listener, errorListener);
//    }
//
//    public static BaseRequest newPostRequest(String url, Map<String, String> params, Response.Listener<T> listener, Response.ErrorListener errorListener) {
//        BaseRequest baseStringRequest = new BaseRequest(Method.POST, url, listener, errorListener);
//        if (params != null && !params.isEmpty()) {
//            baseStringRequest.params.putAll(params);
//        }
//        return baseStringRequest;
//    }
    public BaseRequest(int method, String url, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(method, url, null, listener, errorListener);
    }

    public BaseRequest(int method, String url, Map<String, String> headers, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.mHeaders = headers == null ? new HashMap<String, String>() : headers;
        this.listener = listener;
        setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return mHeaders;
    }

    @Override
    protected void deliverResponse(T response) {
        if (listener != null) {
            listener.onResponse(response);
        }
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        Log.i("parseNetworkError", "call");
        if (volleyError != null) {
            Log.i("volleyError1", volleyError.getMessage()+"");
            if (volleyError.networkResponse != null) {
                Log.i("volleyError2", volleyError.networkResponse.toString());
            }
        }
        return super.parseNetworkError(volleyError);
    }

    public Cache.Entry parseIgnoreCacheHeaders(NetworkResponse response) {
        long now = System.currentTimeMillis();

        Map<String, String> headers = response.headers;
        long serverDate = 0;
        String serverEtag = null;
        String headerValue;

        headerValue = headers.get("Date");
        if (headerValue != null) {
            serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
        }

        serverEtag = headers.get("ETag");


        long cacheHitButRefreshed = 0; // in 3 minutes cache will be hit, but also refreshed on background
        long cacheExpired = 0; // in 24 hours this cache entry expires completely
        long softExpire = 0;
        long ttl = 0;
        if (shouldCache()) {
            cacheHitButRefreshed = 5 * 60 * 1000; // in 5 minutes cache will be hit, but also refreshed on background
            cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            softExpire = now + cacheHitButRefreshed;
            ttl = now + cacheExpired;
        }

        Cache.Entry entry = new Cache.Entry();
        entry.data = response.data;
        entry.etag = serverEtag;
        entry.softTtl = softExpire;
        entry.ttl = ttl;
        entry.serverDate = serverDate;
        entry.responseHeaders = headers;

        return entry;
    }

    protected Cache.Entry enforceClientCaching(Cache.Entry entry,
                                               NetworkResponse response) {
        if (getClientCacheExpiry() == null) return entry;

        long now = System.currentTimeMillis();

        if (entry == null) {
            entry = new Cache.Entry();
            entry.data = response.data;
            entry.etag = response.headers.get("ETag");
            entry.softTtl = now + getClientCacheExpiry();
            entry.ttl = entry.softTtl;
            entry.serverDate = now;
            entry.responseHeaders = response.headers;
        } else if (entry.isExpired()) {
            entry.softTtl = now + getClientCacheExpiry();
            entry.ttl = entry.softTtl;
        }

        return entry;
    }

    protected Integer getClientCacheExpiry() {
        return defaultClientCacheExpiry;
    }
}