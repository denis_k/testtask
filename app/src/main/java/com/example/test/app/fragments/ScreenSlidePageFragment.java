package com.example.test.app.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.*;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.example.test.app.BusHelper;
import com.example.test.app.MainActivity;
import com.example.test.app.R;
import com.example.test.app.models.Result;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

public abstract class ScreenSlidePageFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    protected RecyclerView recyclerView;
    protected RecyclerViewAdapter adapter;
    protected SwipeRefreshLayout swipeRefreshLayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_screen_slide_page, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm);
        adapter = new RecyclerViewAdapter();
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setEnabled(false);
        return rootView;
    }

    @Override
    public void onPrepareOptionsMenu(final Menu menu) {
        super.onPrepareOptionsMenu(menu);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        final MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setIconifiedByDefault(false);

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                clearList();
                onSearch(query);
                MenuItemCompat.collapseActionView(item);
                return true;
            }
        };
        searchView.setOnQueryTextListener(textChangeListener);
    }

    protected abstract void onSearch(String query);

    protected void addAll(List<Result> results) {
        adapter.list.addAll(results);
        adapter.notifyDataSetChanged();
    }

    protected void clearList() {
        adapter.list.clear();
    }

    protected void addFragment(Fragment fragment) {
        FragmentManager fragmentManager = getChildFragmentManager();
        fragmentManager.beginTransaction()
                .add(R.id.content, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(null)
                .commit();
    }

    public abstract class OnPageLoadListener extends RecyclerView.OnScrollListener {
        int pastVisibleItems, visibleItemCount, totalItemCount;

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            visibleItemCount = recyclerView.getLayoutManager().getChildCount();
            totalItemCount = recyclerView.getLayoutManager().getItemCount();
            pastVisibleItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                loadMore(recyclerView.getAdapter().getItemCount());
            }
        }

        public abstract void loadMore(int count);
    }

    protected class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder> {
        private List<Result> list = new ArrayList<Result>();

        public List<Result> getList() {
            return list;
        }

        @Override
        public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
            ItemViewHolder viewHolder = new ItemViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ItemViewHolder personViewHolder, int i) {
            final Result item = list.get(i);
            personViewHolder.position = i;
            ImageLoader.getInstance().displayImage(item.thumbnailUrl, personViewHolder.image);
            personViewHolder.name.setText(item.name);
            personViewHolder.setChecked(item.isFav);
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ItemViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
            @Bind(R.id.image)
            ImageView image;
            @Bind(R.id.title)
            TextView name;
            @Bind(R.id.check)
            CheckBox check;
            int position;

            public ItemViewHolder(View view) {
                super(view);
                ButterKnife.bind(this, view);
                check.setOnCheckedChangeListener(this);
                image.setOnClickListener(this);
            }

            public void setChecked(boolean checked) {
                check.setOnCheckedChangeListener(null);
                check.setChecked(checked);
                check.setOnCheckedChangeListener(this);
            }

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Result item = list.get(position);
                RuntimeExceptionDao<Result, Integer> dao = ((MainActivity) getActivity()).getHelper().getResultDao();
                if (isChecked) {
                    dao.create(item);
                } else {
                    dao.delete(item);
                }
                item.isFav = isChecked;
                BusHelper.INSTANCE.post(new BusHelper.OnFavoriteChanged(item));

            }

            @Override
            public void onClick(View v) {
                Result item = list.get(position);
                addFragment(InfoFragment.newInstance(item.imageUrl));
            }
        }
    }
}